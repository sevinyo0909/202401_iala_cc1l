# Introducción a los Algoritmos

## Reto 4.1

Implemente una función que permita generar un arreglo de tamaño aleatorio entre 100 y 500 y elementos aleatorios entre 1 y 10000. Incluya código de prueba.

## Reto 4.2

Implemente una función que permita crear una matrix dinámica de n x m, la inicialice con ceros y una función que imprima la matrix en pantalla.